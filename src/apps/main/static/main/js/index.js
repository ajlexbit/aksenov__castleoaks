//======================
//         First slider
//----------------------
(function($) {

    $(document).ready(function() {

        Slider('.slider', {
                sliderHeight: Slider.prototype.HEIGHT_CURRENT,
                loop: false,
                itemsPerSlide: 1
            }).attachPlugins([
                SliderSideAnimation({
                    margin: 20
                }),
                SliderSideShortestAnimation({
                    margin: 20
                }),
                SliderFadeAnimation(),
                SliderControlsPlugin({
                    animationName: 'side-shortest'
                }),
                SliderNavigationPlugin({
                    animationName: 'side'
                }),
                SliderDragPlugin({
                    margin: 20
                }),
                SliderAutoscrollPlugin({
                    animationName: 'fade',
                    direction: 'random',
                    interval: 6000
                                
                
                })
            ]);


    });

})(jQuery);


//=======================
//         second slider
//-----------------------
(function($) {

    $(document).ready(function() {

        Slider('#room .slider-room', {
                sliderHeight: Slider.prototype.HEIGHT_CURRENT,
                loop: false,
                itemsPerSlide: 1
            }).attachPlugins([
                SliderSideAnimation({
                    margin: 20
                }),
                SliderSideShortestAnimation({
                    margin: 20
                }),
                SliderFadeAnimation(),
                SliderControlsPlugin({
                    animationName: 'side-shortest'
                }),
                SliderNavigationPlugin({
                    animationName: 'side'
                    
                }),
                SliderDragPlugin({
                    margin: 20
                }),
                SliderAutoscrollPlugin({
                    animationName: 'fade',
                    direction: 'random',
                    interval: 6000
                                
                
                })
            ]);


    });

})(jQuery);


//======================
//         Geolocaton
//----------------------
(function ($) {

    var gmap;

    // получение точки по jQuery-объекту адреса
    var pointByAddr = function ($address) {
        var addr_data = $address.data();
        return GMapPoint(addr_data.lat, addr_data.lng);
    };

    $(document).ready(function () {

        gmap = GMap('#google-map .map', {
            center: pointByAddr($('.map')),
            zoom: 15
        }).on('ready', function () {
            GMapMarker({
                map: this,
                position: this.center(),
            });
        }).on('resize', function () {
            var marker = this.markers[0];
            if (marker) {
                this.center(marker);

                // маркер по центру непокрытой области
                this.panBy(0, -$('.overlay').outerHeight() / 2);
            }
        });
    });

})(jQuery);





/*var map;
   function initMap() {
     var myLatLng = {lat: 40.09007245, lng: -75.14585137};

     var map = new google.maps.Map(document.getElementById('map'), {
       zoom: 18,
       center: myLatLng,
       streetViewControl: false,
       navigationControl: false,
       mapTypeControl: false,
       scaleControl: false


     });

     var marker = new google.maps.Marker({
       position: myLatLng,
       map: map,
     });
   } */

